<h1 align="center">Reus-e</h1>
<h4 align = "center">Reus-e front-end application. Reus-e is a green business marketplace, that you can sell old electronics!</h4>

## Getting started!
```bash
# Clone this repository
$ git clone https://gitlab.com/rafaelbernardino/trabalho-react.git

# Go into the repository
$ cd trabalho-react

# Installing dependencies
$ yarn

# Start the application!
$ yarn start
```